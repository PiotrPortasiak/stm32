 #ifndef _U8G_ARM_H
 #define _U8G_ARM_H

#include "stm32f3xx_hal.h"
#include "gpio.h"

#include "u8g2.h"

extern SPI_HandleTypeDef hspi3;

#define lcd_spi_hanlder hspi3

//#define LCD_CS_GPIO_Port 0
//#define LCD_CS_Pin 0
//#define LCD_DC_GPIO_Port 0
//#define LCD_DC_Pin 0
//#define LCD_RST_GPIO_Port 0
//#define LCD_RST_Pin 0

uint8_t u8x8_stm32_gpio_and_delay(U8X8_UNUSED u8x8_t *u8x8,
     U8X8_UNUSED uint8_t msg, U8X8_UNUSED uint8_t arg_int,
     U8X8_UNUSED void *arg_ptr);
uint8_t u8x8_byte_4wire_hw_spi(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int,
    void *arg_ptr);


// uint8_t u8g_com_hw_spi_fn(u8g2_t *u8g, uint8_t msg, uint8_t arg_val, void *arg_ptr);

 #endif
