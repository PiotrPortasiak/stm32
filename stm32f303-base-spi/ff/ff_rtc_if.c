/********************************************************************************/
/*!
	@file			ff_rtc_if.c
	@author         Nemui Trinomius (http://nemuisan.blog.bai.ne.jp)
    @version        4.00
    @date           2019.10.01
	@brief          FatFs Realtime Clock Section.					@n
					Hardware Abstraction Layer.

    @section HISTORY
		2012.08.27	V1.00	Start Here.
		2015.07.20	V2.00	Add disk_timerproc() for FatFs examples.
		2019.09.01	V3.00	Add weak symbol on disk_timerproc().
		2019.10.01	V4.00	Fixed shadowed warning.

    @section LICENSE
		BSD License. See Copyright.txt
*/
/********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "ff_rtc_if.h"

/* Defines -------------------------------------------------------------------*/

/* Variables -----------------------------------------------------------------*/

/* Constants -----------------------------------------------------------------*/

/* Function prototypes -------------------------------------------------------*/

/* Functions -----------------------------------------------------------------*/

/**************************************************************************/
/*! 
    @brief  FatFs Realtime Clock Section.
	@param  RTC : rtc structure
    @retval : 1
*/
/**************************************************************************/
int rtc_gettime(FF_RTC *f_rtc)
{

#if defined(USE_REDBULL) || defined(USE_ECH_BOARD)
	/* See rtc_support.h */
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);  
	RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);

	f_rtc->sec   = ts_sec;
	f_rtc->min   = ts_min;
	f_rtc->hour  = ts_hour;
	f_rtc->mday  = ts_mday;
	f_rtc->month = ts_mon;
	f_rtc->year  = (ts_year + 1980);	/* Valid for AC1980 ~ AC2079 */

#else /* STM32F4-Discovery and STM32F429I-Disco */
	/* 2019/10/01 11:22:33 */
	f_rtc->sec   = 33;
	f_rtc->min   = 22;
	f_rtc->hour  = 11;
	f_rtc->mday  =  1;
	f_rtc->month = 10;
	f_rtc->year  = 2019;
#endif

	return 1;
}

/**************************************************************************/
/*! 
    @brief  FatFs Realtime Clock Section.
	@param  RTC : rtc structure
    @retval : 1
*/
/**************************************************************************/
int rtc_settime(const FF_RTC *f_rtc)
{

#if defined(USE_REDBULL) || defined(USE_ECH_BOARD)
	/* See rtc_support.h */
	ts_sec	= f_rtc->sec;
	ts_min  = f_rtc->min;
	ts_hour = f_rtc->hour;
	ts_mday = f_rtc->mday;
	ts_mon  = f_rtc->month;
	ts_year = f_rtc->year - 1980;	/* Valid for AC1980 ~ AC2079 */

	RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure);  
	RTC_SetDate(RTC_Format_BIN, &RTC_DateStructure); 

#else /* STM32F4-Discovery and STM32F429I-Disco */
	/* 2013/11/30 13:34:56 */
	/* Do nothing */
#endif

	return 1;
}

/**************************************************************************/
/*! 
    @brief  Device Timer Interrupt Procedure.					@n
			This function must be called in period of 10ms 
	@param  none 
    @retval : none
*/
/**************************************************************************/
/* 100Hz decrement timer stopped at zero (disk_timerproc()) */
volatile uint32_t Timer1, Timer2;  
 __attribute__((weak)) void disk_timerproc(void)
{
	volatile uint32_t n;

	n = Timer1;						/* 100Hz decrement timer */
	if (n) Timer1 = --n;
	n = Timer2;
	if (n) Timer2 = --n;

}

/* End Of File ---------------------------------------------------------------*/
